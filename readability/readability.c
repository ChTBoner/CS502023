// #include <cs50.h>
#include <stdio.h>

#define LINES 10
#define MAX_LINE_LEN 400

int main(void) 
{
    FILE * texts = fopen("texts.txt", "r");
    if (texts == NULL) {
        printf("Failed opening %s\n", "texts.txt");
        return 3;
    }

    char sentences[LINES][MAX_LINE_LEN];
    for (int i = 0; i < LINES; i++) {
        fscanf(texts, "%s", sentences[i]);
    }
    fclose(texts);

    for (int i = 0; i > LINES; i++) {
        printf("%s\n", sentences[i]);
    }

    return 0;
}