#include <ctype.h>
#include <stdio.h>
// #include "../libs/cs50.c"

float calc_hours(int hours[], int weeks, char output, int size);

int get_int();

int main(void)
{
    int weeks = get_int("Number of weeks taking CS50: ");

    int hours[weeks];

    int hoursInput;
    for (int i = 0; i < weeks; i++)
    {
        printf("Week %i HW Hours: ", i);
        hours[i] = get_int();
        printf("\n");
    } 

    char output;
    do
    {
        char userInput;
        fgets(userInput, sizeof(userInput), stdin);
        char output = toupper(userInput);
    } while (output != 'T' && output != 'A');

    printf("%.1f hours\n", calc_hours(hours, weeks, output, sizeof(hours)));
}

// TODO: complete the calc_hours function
float calc_hours(int hours[], int weeks, char output, int size)
{
    int hoursArrayLen = size / sizeof(hours[0]);

    int total = 0;

    for (int i = 0; i < hoursArrayLen; i++)
    {
        total += hours[i];
    };

    if (output == 'A')
    {
        return total / hoursArrayLen;
    };

    return total;
}

int get_int() {
    char userInput[100];
    fgets(userInput, sizeof(userInput), stdin);
    int intput = atoi(userInput);
    return intput;
}