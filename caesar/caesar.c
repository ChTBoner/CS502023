// #include <cs50.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int MAXLEN = 256;


int cipher(int orig, int key);

int error_message();

int main(int argc, char *argv[]) 
{   
    if (argc != 2) {
        printf("Usage: ./caesar key\n");
        return error_message();
    }
    
    int i = 0;

    while (argv[1][i] != '\0')
    {
        if (!isdigit(argv[1][i])) {
            return error_message();
        }
        i++;
    }

    int key = atoi(argv[1]);

    printf("plaintext: ");
    char message[MAXLEN];
    fgets(message, sizeof(message), stdin);

    int j = 0;
    while (message[j] != '\n')
    {
        if (isalpha(message[j]))
        {
            // printf("%i -> %c - %i -> %i - %c\n", j, message[j], message[j], ciphered, ciphered);
            message[j] = cipher(message[j], key);;
        }
        j++;
    }

    printf("ciphertext: %s\n", message);

    return 0;
}

int cipher(int orig, int key) {
    int ciphered = (toupper(orig) - 65 + key) % 26 + 65;
    if islower(orig) {
        return tolower(ciphered);
    }

    return ciphered;
}

int error_message() {
    printf("Usage: ./caesar key\n");
    return 1;   
}