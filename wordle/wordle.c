#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

// each of our text files contains 1000 words
#define LISTSIZE 1000

// values for colors and score (EXACT == right letter, right place; CLOSE == right letter, wrong place; WRONG == wrong letter)
#define EXACT 2
#define CLOSE 1
#define WRONG 0

#define MAX_TRIES 6

// ANSI color codes for boxed in letters
#define GREEN   "\e[38;2;255;255;255;1m\e[48;2;106;170;100;1m"
#define YELLOW  "\e[38;2;255;255;255;1m\e[48;2;201;180;88;1m"
#define RED     "\e[38;2;255;255;255;1m\e[48;2;220;20;60;1m"
#define RESET   "\e[0;39m"

char * get_guess(int size);
int check_word(char *guess, char *solution, int *status, int wordsize, int attempts);
int check_score(char guess, char *solution, int position);

int main(int argc, char* argv[]) {
    // check usage
    if (argc != 2) {
        printf("Usage: ./wordle number\n");
        return 1;
    }

    int wordsize = atoi(argv[1]);
    if (wordsize < 5 || wordsize > 8) {
        printf("Number must be between 5 and 8\n");
        return 2;
    }
    
    // pick a word randomly in the corresponding file
    char filename[6];
    sprintf(filename, "%i.txt", wordsize);
    FILE *wordfile = fopen(filename, "r");
    if (wordfile == NULL)
    {
        printf("Failed opening %s\n", filename);
        return 3;
    }

    char listOfWords[LISTSIZE][wordsize+1];

    for (int i = 0; i < LISTSIZE; i++) {
        fscanf(wordfile, "%s", listOfWords[i]);
    }
    fclose(wordfile);

    srand(time(NULL));
    int randNum = rand() % LISTSIZE;
    char * solution = listOfWords[randNum];

    int result = WRONG;
    int status[wordsize];
    int attempts = 1;
    printf("You have %d tries to guess the %d-letter word I'm thinking of\n", MAX_TRIES, wordsize);
    while (result != EXACT && attempts <= MAX_TRIES) {
        char * guess = get_guess(wordsize);
        result = check_word(guess, solution, status, wordsize, attempts);
        attempts++;
    }
    
    if (result != EXACT) {
        printf("The answer was '%s!'\n", solution);
    } else {
        printf("You won!\n");
    }

    return 0;
}

char * get_guess(int size) {
    char *userInput = malloc(size + 1);
    while (strlen(userInput) - 1 != size) {
        printf("Input a 5-letter word: ");
        fgets(userInput, sizeof(userInput), stdin);
    }

    return userInput;

}

int check_word(char *guess, char *solution, int *status, int size, int attempt){
    // generate word status
    printf("Guess %d: ", attempt);
    for (int i = 0; i < size; i++) {
        status[i] = check_score(guess[i], solution, i);
        switch (status[i])
        {
        case EXACT:
            printf(GREEN"%c"RESET, guess[i]);
            break;
        
        case CLOSE:
            printf(YELLOW"%c"RESET, guess[i]);
            break;
        
        case WRONG:
            printf(RED"%c"RESET, guess[i]);
            break;
        }
    }
    printf("\n");

    for (int i = 0; i < size; i++)
    {
        if (status[i] != EXACT) {
            return WRONG;
        }
    }
    return EXACT;

}

int check_score(char guess, char *solution, int position) {
    // check if letter is at right position
    if (guess == solution[position]) {
        return EXACT;
    }
    
    // check if guess is in word
    int i = 0;
    while (solution[i] != '\0')
    {
        if (guess == solution[i]) {
            return CLOSE;
        }
        i++;
    }

    return WRONG;

}