#include <ctype.h>
#include <stdio.h>
#include <string.h>
// #include "../libs/cs50.h"

// Points assigned to each letter of the alphabet
int POINTS[] = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};

int compute_score(char word[]);

int main(void)
{
    // Get input words from both players
    char word1[100], word2[100];
    printf("Player 1: \n");
    scanf("%s", word1);
    printf("Player 2: \n");
    scanf("%s", word2);

    // Score both words
    int score1 = compute_score(word1);
    int score2 = compute_score(word2);

    // TODO: Print the winner
    if (score1 > score2) 
    {
        printf("Player 1 wins!\n");
    }
    else if (score1 < score2)
    {
        printf("Player 2 wins!\n");
    }
    else 
    {
        printf("Tie!\n");
    }

    return 0;
}

int compute_score(char word[])
{
    int score = 0;
    
    int i = 0;
    while ( word[i] != '\0')
    {
        int index = toupper(word[i]) - 65;
        int points = POINTS[index];
        printf("word[%i] = %c => index = %i => points = %i\n", i, word[i], index, points);
        score += POINTS[index];
        i++;
    }
    printf("%i\n", score);

    return score;
}
