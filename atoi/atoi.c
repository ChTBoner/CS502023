// #include <cs50.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

int convert(char* input);

int main(void)
{
    char input[255];
    printf("Enter a positive integer: ");
    fgets(input, sizeof(input), stdin);
    // remove the carriage return from input

    input[strlen(input) - 1 ] = '\0';

    for (int i = 0, n = strlen(input); i < n; i++)
    {
        if (!isdigit(input[i]))
        {
            printf("Invalid Input!\n");
            return 1;
        }
    }

    // Convert char* to int
    printf("%i\n", convert(input));
}

int convert(char* input)
{
    int powered = strlen(input);
    int converted = 0;

    for (int i = 0; i < powered; i++ ) {
        converted += (input[i] - 48 )*(int)pow(10, powered - (i+1));
    }
    return converted;
}