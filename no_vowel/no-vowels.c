// Write a function to replace vowels with numbers
// Get practice with strings
// Get practice with command line
// Get practice with switch

#include <stdio.h>
// #include "../libs/cs50.h"

void replace(char* replaced);

int main(int argc, char* argv[])
{
    if (argc != 2) {
        printf("Should have 1 argument!\n");
        return 1;
    }
    replace(argv[1]);
    printf("%s\n", argv[1]);
    return 0;
}

void replace(char* replaced)
{   
    int i = 0;
    while (replaced[i] != '\0')
    {
        switch (replaced[i])
        {
            case 'a':
                replaced[i] = '6';
                break;
            case 'e':
                replaced[i] = '3';
                break;
            case 'i':
                replaced[i] = '1';
                break;
            case 'o':
                replaced[i] = '0';
                break;
        }
        i++;
    }
}