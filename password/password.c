// Check that a password has at least one lowercase letter, uppercase letter, number and symbol
// Practice iterating through a string
// Practice using the ctype library

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int valid(char* password);

int main(int argc, char* argv[])
{
    // Usage check
    if (argc != 2) {
        printf("Should have 1 argument!\n");
        return 1;
    }
    
    if (valid(argv[1]))
    {
        printf("Your password is valid!\n");
        return 0;
    }
    else
    {
        printf("Your password needs at least one uppercase letter, lowercase letter, number and symbol\n");
        return 1;
    }
}

// TODO: Complete the Boolean function below
int valid(char* password)
{
    int truthTable[4] = { 0, 0, 0, 0 };

    for (int i = 0; i < strlen(password); i++)
    {
        for (int i = 0; i < 4; i++) {
        }
        if ( truthTable[0] == 0 ) {
                truthTable[0] = isupper(password[i]);
        }
        
        if ( truthTable[1] == 0 ) {
                truthTable[1] = islower(password[i]);
        }
        
        if ( truthTable[2] == 0) {
                truthTable[2] = isdigit(password[i]);
        }

        if ( truthTable[3] == 0) {
                if (!isalnum(password[i])) {
                    truthTable[3] = 1;
                }
        }
    }

    // parse truthtable
    for (int i = 0; i < 4; i++) {
        if ( truthTable[i] == 0 ) {
            return 0;
        }
    }
    return 1;
}
