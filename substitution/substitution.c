// #include "../libs/cs50.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int cipher(int orig, char subs[]);
int charIndex(int letter);

int main(int argc, char *argv[]) 
{
    if (argc != 2 || strlen(argv[1]) != 26)
    {
        printf("Key must be contain 26 characters\n");
        return 1;
    }

    char plaintext[256];
    printf("plaintext: ");
    fgets(plaintext, sizeof(plaintext), stdin);
    // printf("%s\n",  );

    int length = strlen(plaintext) -1 ;

    for (int i = 0 ; i < length ; i++ )
    {
        if (isalpha(plaintext[i])) {
            plaintext[i] = cipher(plaintext[i], argv[1]);
        }

    }
    printf("ciphertext: %s", plaintext);

    return 0;
}

int cipher(int orig, char subs[]) {
    int subed = subs[charIndex(orig)];
    if islower(orig) {
        subed = tolower(subed);
    }

    return subed;
}

int charIndex(int letter) {
    return toupper(letter) - 65;
}