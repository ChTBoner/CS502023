// #include <cs50.h>
#include <stdio.h>
#include <string.h>

const int BITS_IN_BYTE = 8;

void print_bulb(int bit);

int main(void)
{
    char message[100];
    printf("Message:\n");
    fgets(message, sizeof(message), stdin);

    int i = 0;
    while (message[i] != '\n')
    {
        int bits[8] = {0, 0, 0, 0, 0, 0, 0, 0};

        // printf("%i\n", message[i]);
        int num = message[i];
        int y = 7;
        while (num != 0) {
            if (num % 2 == 1) bits[y] = 1;
            num /= 2;
            y--;
        }
        
        for (int j = 0; j < 8; j++)
        {
            print_bulb(bits[j]);
        }

        i++;
        printf("\n");
    }
}

void print_bulb(int bit)
{
    if (bit == 0)
    {
        // Dark emoji
        printf("\U000026AB");
    }
    else if (bit == 1)
    {
        // Light emoji
        printf("\U0001F7E1");
    }
}
